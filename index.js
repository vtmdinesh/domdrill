// Select the section with an id of container without using querySelector.

let containerEl = document.getElementById("container")

//console.log(containerEl)

// Select the section with an id of container using querySelector.

let containerEl2 = document.querySelector("#container")

// console.log(container)

// Select all of the list items with a class of "second".

let itemsWithSecond = document.querySelectorAll(".second")
//console.log(itemsWithSecond)

// Give the section with an id of container the text "Hello!".

let thirdListEl = document.querySelector("ol .third")

// console.log(thirdListEl)

//Give the section with an id of container the text "Hello!".

document.getElementById("container").innerHTML += "Hello!"

x = (document.getElementById("container").children)
console.log(x)

// Add the class main to the div with a class of footer.

document.querySelector(".footer").classList.add("main")

//Remove the class main on the div with a class of footer.


document.querySelector(".footer").classList.remove("main")

// Create a new li element.

let newListItem = document.createElement("li")

//Give the li the text "four".

newListItem.textContent = "four"

//Append the li to the ul element. 

document.querySelector("ul").appendChild(newListItem)

// Loop over all of the lis inside the ol tag and give them a background color of "green".

let childsOfOl = document.querySelector("ol").children

for (let index = 0; index < childsOfOl.length; index++) {
    childsOfOl[index].style.backgroundColor = "green";
}

//Remove the div with a class of footer.

let footerEl = document.querySelector("div.footer")

document.querySelector("body").removeChild(footerEl)